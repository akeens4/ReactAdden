import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import logo from './logo.svg';
import './App.css';


import Home from './Pages/Home.jsx';
import contact from './Pages/contact.jsx';
import gallery from './Pages/gallery.jsx';
class App extends Component {
  render() {
    return (
      <Router>
        <div>
<Route exact strict path="/" component={Home}/>
<Route path="/contact" component={contact}/>
<Route path="/gallery" component={gallery}/>
        </div>
      </Router>
    );
  }
}

export default App;
