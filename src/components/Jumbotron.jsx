import React, { Component } from 'react';
import './Jumbotron.css';

class Jumbotron extends Component {
    render() {
        return (
            <div className="jumbotron jumbotron-fluid">
            <div className="container">
            <h1 className="display-3">{this.props.title}</h1>
            <p className="lead">{this.props.subtitle}</p>
          <p>AddenFlix is a movie rental store where viewer can buy, 
	rent and download any movie of their choice. 
	We are ready to satisfy our customers and make ready the 
	latest movies in Hollywood, Nollywood, Tollywood and the likes.</p>
            </div>
            </div>
        );
    }
}

export default Jumbotron