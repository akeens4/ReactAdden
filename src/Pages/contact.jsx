import React, { Component } from 'react';
import Navbar from '../components/Navbar.jsx';
import Footer from '../components/Footer.jsx'; 
import Jumbotron from '../components/Jumbotron.jsx';
import Body from '../components/Body.jsx';
import './contact.css';


class contact extends Component {
    render() {
        return (
<div>
            <Navbar />
            
            <div className="container">
            <br />
<form class="form1">
<h1>Contact Form</h1>
<h3 id="showFormValues"></h3> 
<br />
<div class="form-group">
    <label for="exampleInputEmail1" >Email Address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" arial-describedby="emailHelp" placeholder="Enter email" />
    <small id="email_error" class="red-text flag-error"></small>
</div>

<div class="form-group">
    <label for="exampleInputFullname">Full Name</label>
    <input type="text" class="form-control" id="exampleInputFullname" arial-describedby="emailFullname" placeholder="Enter Full name" />
    <small id="fullname_error" class="red-text flag-error"></small>
</div>

<div class="form-group">
    <label for="exampleInputMessage" >Message</label>
    <textarea type="text" class="form-control" id="exampleInputMessage" rows="3" ></textarea>
    <small id="message_error" class="red-text flag-error"></small>
</div>

<span id="submitForm" class="btn btn-primary">Send</span>
<hr />
<span id="toggleBtn" class="btn btn-xl btn-warning">Hide/Show</span> 
</form>
</div>
<Footer />
</div>
        );
    }
}

export default contact
