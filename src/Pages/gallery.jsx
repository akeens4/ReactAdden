import React, { Component } from 'react';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer'; 
import Jumbotron from '../components/Jumbotron';
import Body from '../components/Body';
import '../components/Body.css';
import './gallery.css';

import img5 from '../images/mission.jpg';
import img6 from '../images/mowgli.jpg';
import img8 from '../images/readyPlayerOne.jpg';
import img16 from '../images/power-rangers.jpg';
import img25 from '../images/rings.jpg';
import img26 from '../images/the-Fate-Of-The-Furious.jpg';
import img27 from '../images/Resident-Evil-The-Final-Chapter.jpg';
import img28 from '../images/tomb-Raider.jpg';
import img29 from '../images/The-Hitmans-Bodyguard.jpg';
import img30 from '../images/the-Mummy.jpg';
import img31 from '../images/the-Dark-Tower.jpg';
import img32 from '../images/Black-Panther.jpg';
import img33 from '../images/Robin-Hood-Origins.jpg';
import img3 from '../images/pirates-Of-The-Caribbean-Dead-Men-Tell-No.jpg';
import img34 from '../images/the-Predator.jpg';
import img35 from '../images/The-Disaster-Artist.jpg';
import img36 from '../images/Thor-Ragnarok.jpg';
import img37 from '../images/Alien-Covenant.jpg';
import img38 from '../images/Beauty-And-The-Beast.jpg';
import img39 from '../images/Baywatch.jpg';

class gallery extends Component {
    render() {
        return (
            <div>
            <Navbar />
<div classNameName="container">
            <h2 fontFamily={'arial helvetica'} textAlign= {'center'}> 2018 Movies</h2>
            <h2>Rent movies and watch at your leisure time</h2>
            <div className="container text-center">
            
          <div className="flixcard">
             <div  className="card">
                <img src={img6} alt="Mowgli" />
                 <div className="cost">
                 <h3><strong>Mogwli</strong></h3>
                     <p>
                     <strong textDecoration= {'line-through'}>$</strong>
                     <strong>20</strong>
                     </p>
                 
                 </div>
                </div>
                <br />
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">RENT</button>&nbsp;
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">DOWNLOAD</button>
               </div>

               <div className="flixcard">
             <div  className="card">
                <img src={img5} alt="Mission Impossible" />
                 <div className="cost">
                 <h3><strong>Mission</strong></h3>
                     <p>
                     <strong textDecoration= {'line-through'}>$</strong>
                     <strong>20</strong>
                     </p>
                 
                 </div>
                </div>
                <br />
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">RENT</button>&nbsp;
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">DOWNLOAD</button>
               </div>

            <div className="flixcard">
             <div  className="card">
                <img src={img25} alt="Rings" />
                 <div className="cost">
                 <h3><strong>Rings</strong></h3>
                     <p>
                     <strong textDecoration= {'line-through'}>$</strong>
                     <strong>20</strong>
                     </p>
                 
                 </div>
                </div><br />
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">RENT</button>&nbsp;
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">DOWNLOAD</button>
               </div>

          <div className="flixcard">
             <div  className="card">
                <img src={img16} alt="Power Rangers" />
                 <div className="cost">
                 <h3><strong>Power Rangers</strong></h3>
                     <p>
                     <strong textDecoration= {'line-through'}>$</strong>
                     <strong>20</strong>
                     </p>
                 
                 </div>
                </div><br />
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">RENT</button>&nbsp;
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">DOWNLOAD</button>
               </div>
          
              
              
               

               <div className="flixcard">
             <div  className="card">
                <img src={img8} alt="Ready Player One" />
                 <div className="cost">
                 <h3><strong>Ready Player One</strong></h3>
                     <p>
                     <strong textDecoration= {'line-through'}>$</strong>
                     <strong>20</strong>
                     </p>
                 
                 </div>
                </div>
                <br />
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">RENT</button>&nbsp;
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">DOWNLOAD</button>
               </div>
                            
               <div className="flixcard">
             <div  className="card">
                <img src={img3} alt="Ready Player One" />
                 <div className="cost">
                 <h3><strong>Deadpool</strong></h3>
                     <p>
                     <strong textDecoration= {'line-through'}>$</strong>
                     <strong>20</strong>
                     </p>
                 
                 </div>
                </div>
                <br />
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">RENT</button>&nbsp;
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">DOWNLOAD</button>
               </div>
          
               <div className="flixcard">
             <div  className="card">
                <img src={img26} alt="Ready Player One" />
                 <div className="cost">
                 <h3><strong>FastnFurious</strong></h3>
                     <p>
                     <strong textDecoration= {'line-through'}>$</strong>
                     <strong>35</strong>
                     </p>
                 
                 </div>
                </div>
                <br />
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">RENT</button>&nbsp;
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">DOWNLOAD</button>
               </div>
          
               <div className="flixcard">
             <div  className="card">
                <img src={img27} alt="Resident Evil" />
                 <div className="cost">
                 <h3><strong>Resident Evil</strong></h3>
                     <p>
                     <strong textDecoration= {'line-through'}>$</strong>
                     <strong>35</strong>
                     </p>
                 
                 </div>
                </div>
                <br />
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">RENT</button>&nbsp;
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">DOWNLOAD</button>
               </div>
            
               <div className="flixcard">
             <div  className="card">
                <img src={img28} alt="Tomb Raider" />
                 <div className="cost">
                 <h3><strong>Tomb Raider</strong></h3>
                     <p>
                     <strong textDecoration= {'line-through'}>$</strong>
                     <strong>35</strong>
                     </p>
                 
                 </div>
                </div>
                <br />
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">RENT</button>&nbsp;
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">DOWNLOAD</button>
               </div>
           
               <div className="flixcard">
             <div  className="card">
                <img src={img29} alt="Pacific Rim" />
                 <div className="cost">
                 <h3><strong>Hitman</strong></h3>
                     <p>
                     <strong textDecoration= {'line-through'}>$</strong>
                     <strong>25</strong>
                     </p>
                 
                 </div>
                </div>
                <br />
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">RENT</button>&nbsp;
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">DOWNLOAD</button>
               </div>
               
               <div className="flixcard">
                <div  className="card">
                <img src={img30} alt="Pacific Rim" />
                 <div className="cost">
                 <h3><strong>The Mummy</strong></h3>
                     <p>
                     <strong textDecoration= {'line-through'}>$</strong>
                     <strong>20</strong>
                     </p>
                 
                 </div>
                </div>
                <br />
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">RENT</button>&nbsp;
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">DOWNLOAD</button>
               </div>
           
               <div className="flixcard">
                <div  className="card">
                <img src={img31} alt="Dark Tower" />
                 <div className="cost">
                 <h3><strong>Dark Tower</strong></h3>
                     <p>
                     <strong textDecoration= {'line-through'}>$</strong>
                     <strong>20</strong>
                     </p>
                 
                 </div>
                </div>
                <br />
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">RENT</button>&nbsp;
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">DOWNLOAD</button>
               </div>

            <div className="flixcard">
                <div  className="card">
                <img src={img32} alt="Disaster Artist" />
                 <div className="cost">
                 <h3><strong>Black Panther</strong></h3>
                     <p>
                     <strong textDecoration= {'line-through'}>$</strong>
                     <strong>20</strong>
                     </p>
                 
                 </div>
                </div>
                <br />
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">RENT</button>&nbsp;
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">DOWNLOAD</button>
               </div>

             <div className="flixcard">
                <div  className="card">
                <img src={img33} alt="Oceans 8" />
                 <div className="cost">
                 <h3><strong>Oceans 8</strong></h3>
                     <p>
                     <strong textDecoration= {'line-through'}>$</strong>
                     <strong>20</strong>
                     </p>
                 
                 </div>
                </div>
                <br />
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">RENT</button>&nbsp;
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">DOWNLOAD</button>
               </div>

              <div className="flixcard">
                <div  className="card">
                <img src={img34} alt="Spider man" />
                 <div className="cost">
                 <h3><strong>SpiderMan</strong></h3>
                     <p>
                     <strong textDecoration= {'line-through'}>$</strong>
                     <strong>20</strong>
                     </p>
                 
                 </div>
                </div>
                <br />
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">RENT</button>&nbsp;
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">DOWNLOAD</button>
               </div>

               <div className="flixcard">
                <div  className="card">
                <img src={img35} alt="Solo star wars" />
                 <div className="cost">
                 <h3><strong>Disaster</strong></h3>
                     <p>
                     <strong textDecoration= {'line-through'}>$</strong>
                     <strong>28</strong>
                     </p>
                 
                 </div>
                </div>
                <br />
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">RENT</button>&nbsp;
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">DOWNLOAD</button>
               </div>
           
               <div className="flixcard">
                <div  className="card">
                <img src={img36} alt="Thor-Ragnarok" />
                 <div className="cost">
                 <h3><strong>Thor-Ragnarok</strong></h3>
                     <p>
                     <strong textDecoration= {'line-through'}>$</strong>
                     <strong>28</strong>
                     </p>
                 
                 </div>
                </div>
                <br />
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">RENT</button>&nbsp;
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">DOWNLOAD</button>
               </div>
               
               <div className="flixcard">
                <div  className="card">
                <img src={img37} alt="Alien-Covenant" />
                 <div className="cost">
                 <h3><strong>Alien Covenant</strong></h3>
                     <p>
                     <strong textDecoration= {'line-through'}>$</strong>
                     <strong>28</strong>
                     </p>
                 
                 </div>
                </div>
                <br />
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">RENT</button>&nbsp;
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">DOWNLOAD</button>
               </div>

               <div className="flixcard">
                <div  className="card">
                <img src={img38} alt="Beauty and The Beast" />
                 <div className="cost">
                 <h3><strong>BeautyBeast</strong></h3>
                     <p>
                     <strong textDecoration= {'line-through'}>$</strong>
                     <strong>28</strong>
                     </p>
                 
                 </div>
                </div>
                <br />
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">RENT</button>&nbsp;
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">DOWNLOAD</button>
               </div>

              <div className="flixcard">
                <div  className="card">
                <img src={img39} alt="Baywatch" />
                 <div className="cost">
                 <h3><strong>Baywatch</strong></h3>
                     <p>
                     <strong textDecoration= {'line-through'}>$</strong>
                     <strong>28</strong>
                     </p>
                 
                 </div>
                </div>
                <br />
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">RENT</button>&nbsp;
                <button type="button" className="btn btn-info my-2 my-sm-0 btn4">DOWNLOAD</button>
               </div>
            <br /><br /><br /> <br />
            <button type="button" className="btn btn-info my-2 my-sm-0 btn4">Click To Expand the Gallery</button>
        </div>
        </div>
            <Footer />
            </div>
        );
    }
}

export default gallery
