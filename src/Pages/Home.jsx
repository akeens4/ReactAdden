import React, { Component } from 'react';
import Navbar from '../components/Navbar.jsx';
import Footer from '../components/Footer.jsx'; 
import Jumbotron from '../components/Jumbotron.jsx';
import Body from '../components/Body.jsx';

class Home extends Component {
    render() {
        return (
            <div>
            <Navbar />
            <Jumbotron title="Welcome" subtitle="AddenFlix is an online film rental services" />
            <Body />
        <div className="container">
           <div>
               <h2>Welcome</h2>
               
           </div> 
           </div>
           <Footer />
           </div>
        );
    }
}
export default Home